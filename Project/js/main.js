document.addEventListener("DOMContentLoaded", main);

function main() {
   scaleTariffs();
}

function scaleTariffs() {
   const scaleItems = document.querySelectorAll('#tariffs-item');

   for (let i = 0; i < scaleItems.length; i++) {
      scaleItems[i].addEventListener('mouseover', (e) => {
         console.log(e.target.childElement)
         scaleItems[i].classList.add('tariffs-item-hover')
      });
   
      scaleItems[i].addEventListener('mouseout', () => {
         scaleItems[i].classList.remove('tariffs-item-hover')
      });
   }
   
}